package com.devcamp.api.j58jpahibernate.controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.cache.internal.EnabledCaching;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.j58jpahibernate.model.CVoucher;
import com.devcamp.api.j58jpahibernate.repository.IVoucherRepository;

@CrossOrigin
@RestController
public class CVoucherController {
    @Autowired
    IVoucherRepository pVoucherRepository;

    @GetMapping("/vouchers")
    public ArrayList<CVoucher> getAllVouchers() {
        ArrayList<CVoucher> lstVouchers = new ArrayList<>();
        pVoucherRepository.findAll().forEach(lstVouchers::add);

        return lstVouchers;
    }

    @GetMapping("/voucher2")
    public ResponseEntity<List<CVoucher>> getAllVoucher2() {
        try {
            ArrayList<CVoucher> lstVouchers = new ArrayList<>();
            pVoucherRepository.findAll().forEach(lstVouchers::add);

            if (lstVouchers.size() == 0) {
                return new ResponseEntity<List<CVoucher>>(lstVouchers, HttpStatus.NOT_FOUND);
            } else {
                return ResponseEntity.status(HttpStatus.OK).body(lstVouchers);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            // TODO: handle exception
        }
    }
}
